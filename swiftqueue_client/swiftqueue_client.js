var Client, client, extradata;

Client = require("request-json").JsonClient;

client = new Client("http://localhost:3015/");

extradata = {
  tag: "username here"
};

client.sendFile('runSimulation/', './radimg-849029453006.tar.gz', extradata, function(err, res, body) {
  if (err) {
    return console.log(err);
  } else {
    return console.log('file uploaded');
  }
});
