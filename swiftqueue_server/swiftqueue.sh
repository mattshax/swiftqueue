#!/bin/bash

# make sure swift is in your path

homedir=$(pwd)

inpath=$homedir/0_incoming
outpath=$homedir/0_outgoing
runpath=$homedir/0_running
savepath=$homedir/save

while true;
do

    echo "watching"

    for incoming in $(ls -1 $inpath/* 2>/dev/null);
    do
        echo "incoming"
        # get the app name and the client id
        IFS="-" read -ra inParams <<< $(basename $incoming)
        appID=${inParams[0]} && clientID=${inParams[1]}

        # execute the correct function based on the appID

        if [[ $appID == "radimg" ]];then

            echo "Running Radiance Image Workflow"
            # move the incoming files to a new run directory
            filename=${incoming##*/} && filename=${filename%.tar.gz} && rundir=$runpath/$filename && mkdir $rundir && tar xzvf $incoming -C $rundir && rm $incoming
            # convert the incoming obj file to a rad file        
            mkdir $rundir/bin && $homedir/bin/radimg/inputs/obj2rad -o obj -f $rundir/$filename.obj > $rundir/$filename.rad
            # copy the swift and radiance template files to rundir
            cp bin/radimg/inputs/* $rundir/bin && cp bin/radimg/swift/* $rundir && cd $rundir
            #start the swift job
            swift rad.swift -rad=$filename.rad -out=$outpath/$filename.gif
            cd $homedir

        elif [[ $appID == "radhr" ]];then

            echo "Running Radiance Hour Workflow"
            # move the incoming files to a new run directory
            filename=${incoming##*/} && filename=${filename%.tar.gz} && rundir=$runpath/$filename && mkdir $rundir && tar xzvf $incoming -C $rundir && rm $incoming
            # convert the incoming obj file to a rad file        
            mkdir $rundir/bin && $homedir/bin/radhr/inputs/obj2rad -o obj -f $rundir/$filename.obj > $rundir/bin/box.rad && mv $rundir/$filename.pts $rundir/bin/box.pts
            # copy the swift and radiance template files to rundir
            cp bin/radhr/inputs/* $rundir/bin && cp bin/radhr/swift/* $rundir && cd $rundir
            #start the swift job
            swift rad.swift && tar czvf $filename.tgz output/ill/*.ill output/crit/*.criteria && mv $filename.tgz $outpath
            cd $homedir

        elif [[ $appID == "ep" ]];then
        
            echo "Running EnergyPlus Workflow"
            
        else 
            echo "No Matching Workflow - removing job"
            rm $incoming
        fi
        
    done
    
    for outgoing in $(ls -1 $outpath/* 2>/dev/null);
    do
    
        echo "outgoing"
        
        # remove from the run directory
        filename=${outgoing##*/} && filename=${filename%.*} && rundir=$runpath/$filename && rm $rundir -R
        # ssh the result file back to user, or generate send an http path for embedding
        cp $outgoing $savepath # save the file for now
        rm $outgoing
    done
    sleep 2s
    
done

