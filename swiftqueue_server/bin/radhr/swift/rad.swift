type file;

# APP CONFIGURATIONS
app (file ill, file crit, file out, file err) runRadiance (file runRad, string id, file rad, file runFiles[] )
{
    bash @runRad @ill @crit stdout=@out stderr=@err;
}

# OUTPUT DEFINITIONS
string outpath=arg("out","output");
file rad_ill[] <simple_mapper; prefix="output/ill/rad", suffix=".ill">;
file rad_crit[] <simple_mapper; prefix="output/crit/rad", suffix=".criteria">;
file rad_out[] <simple_mapper; prefix="output/out/rad", suffix=".out">;
file rad_err[] <simple_mapper; prefix="output/out/rad", suffix=".err">;

# RADIANCE SHELL SCRIPT
file runRadFile <single_file_mapper; file="bin/runIll.sh">;

# INPUT FILES FOR RADIANCE RUN
file rad <single_file_mapper; file=arg("rad","bin/box.rad")>;
file runFiles[] <filesys_mapper;location="bin">;
string runs[] = readData(arg("runs","bin/runs.txt"));

foreach runid,i in runs {
    (rad_ill[i], rad_crit[i], rad_out[i], rad_err[i]) = runRadiance(runRadFile,runid,rad,runFiles);
}
