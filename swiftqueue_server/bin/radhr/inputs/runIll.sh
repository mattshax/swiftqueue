#! /bin/bash

# usage ./runIll.sh <OUTFILE.ill> <RUNDIR>

export PATH=$PATH:/home/som/Radiance/daysim/day_all/bin

outill=$PWD/$1
shift
outcrit=$PWD/$1

rundir=bin
cd $rundir

epw=*.epw
wea=$(basename $epw .epw).wea
hea=*.hea

echo Header:$hea

epw2wea $epw $wea
radfiles2daysim $hea -m -g
gen_dc $hea -dif
gen_dc $hea -dir
gen_dc $hea -paste

ds_illum $hea
RC=$?

if [ $RC = 0 ]; then
  echo Daysim application complete: RC=0
  node sunHours.js > meets.criteria
  ill=*.ill
  cp $ill $outill
  cp meets.criteria $outcrit
else
   echo Daysim application failed: RC=$RC
fi

exit $RC
