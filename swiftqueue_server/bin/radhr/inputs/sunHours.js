var fs = require('fs');

fs.readFile("./box.ill", function(err, content) {
    var data = content.toString().split("\n");
    data.pop(); // remove the last white space
    countHours(data);
})

function countHours(data) {

    // for every 24 hours, count the points that are above the direct sunlight threshold
    // count the number of days per points above the sunlight hr threshold
    // each point should have at least 2 hrs of sunlight per day
    // 1) calculate number of hours above threshold per day per point
    // output - array length 365 with total hrs above threshold
    // 2) reform to a simple 0/1 meets criteria per point

    var hrsPerDay = [];
    var meetsCriteria = [];
    var hrsPerDayThreshold = 2;
    var directThreshold = 10000;

    // instantiate the meetCriteria array
    var ptCount = 0;
    for (var j = 4; j < data[0].split(" ").length; j++) {
        meetsCriteria[ptCount] = 0;
        ptCount = ptCount + 1;
    }

    for (var i = 0; i < data.length / 24; i++) { // loop through each day
        var dayCount = [];
        var ptCount = 0;
        for (var j = 4; j < data[0].split(" ").length; j++) {
            dayCount[ptCount] = 0;
            ptCount = ptCount + 1;
        }
        for (var hr = 0; hr < 15; hr++) { // look at each hour in the day
            var day = data[i * 24 + hr].split(" ");
            var ptCount = 0;
            for (var j = 4; j < day.length; j++) { // look at each point on the hour
                if (parseInt(day[j]) > directThreshold) {
                    dayCount[ptCount] = dayCount[ptCount] + 1;
                }
                ptCount = ptCount + 1;
            }
        }
        hrsPerDay.push(dayCount);
    }

    var ptCount = 0;
    for (var j = 4; j < data[0].split(" ").length; j++) {
        var critCount = 0;
        for (var k = 0; k < hrsPerDay.length; k++) {
            if (hrsPerDay[k][ptCount] >= hrsPerDayThreshold) {
                critCount = critCount + 1;
            }
        }
        // console.log(critCount);
        if (critCount >= 360) { //meets criteria
            meetsCriteria[ptCount] = 1;
        }
        ptCount = ptCount + 1;
    }

    console.log(meetsCriteria.join(" "));

}
