#!/bin/bash

export PATH=$PATH:/home/som/rad

mv $2 bin
cd bin

time=$1
infile=$2
outfile=$3

pos="-435.82 -231.03 452.73" # fixed camera position for now
dir="387.37 348.45 -439.65" # fixed camerea direction for now

IFS=' ' read -ra posA <<< "$pos"
IFS=' ' read -ra dirA <<< "$dir"

GenCumulativeSky +s1 -a 61.18 -o 150 -m 135 -r -E -time $time -date 6 21 6 21 box.epw > box.cal
oconv -f box_gcsky.rad boxmaterial.rad $infile > box.oct
rpict -t 15 -i -ab 2 -ad 1000 -as 20 -ar 300 -aa 0.1 -vtv -vp ${posA[0]} ${posA[1]} ${posA[2]} -vd ${dirA[0]} ${dirA[1]} ${dirA[2]} -vu 0 0 1 -vh 26.24 -vv 26.99 -vs 0 -vl 0  -x 600 -y 400 box.oct > box_kwhm-2.pic
csh falsecolor2.csh -i box_kwhm-2.pic -s .4 -n 10 -l Whm-2 -mask 0.00001 > box_fc.pic
ra_bmp box_fc.pic output.bmp
mv output.bmp ../$outfile