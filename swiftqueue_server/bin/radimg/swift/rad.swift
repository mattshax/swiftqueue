type file;

# APP CONFIGURATIONS
app (file img, file out, file err) runRadiance (file runRad, string id, file rad, file runFiles[] )
{
    bash @runRad id @rad @img stdout=@out stderr=@err;
}

app (file o) convert (file s[])
{
    convert "-delay" 50 filenames(s) filename(o);
}

# OUTPUT DEFINITIONS
string outpath=arg("out","output");
file rad_img[] <simple_mapper; prefix="output/img/rad", suffix=".bmp">;
file rad_out[] <simple_mapper; prefix="output/out/rad", suffix=".out">;
file rad_err[] <simple_mapper; prefix="output/out/rad", suffix=".err">;

# RADIANCE SHELL SCRIPT
file runRadFile <single_file_mapper; file="bin/runRadiance.sh">;

# INPUT FILES FOR RADIANCE RUN
file rad <single_file_mapper; file=arg("rad","bin/box.rad")>;
file runFiles[] <filesys_mapper;location="bin">;
string runs[] = readData(arg("times","bin/times.txt"));

foreach runid,i in runs {
    (rad_img[i], rad_out[i], rad_err[i]) = runRadiance(runRadFile,runid,rad,runFiles);
}

file movie <single_file_mapper; file=outpath>;
movie = convert(rad_img);