var app, express, fs;

express = require('express');
fs = require('fs');

var busboy = require('connect-busboy');

app = express();

app.use(busboy({ immediate: true }));

var server = require('http').Server(app);


app.use(function(req, res) {
    req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
        var path="./0_incoming/"+filename;
        file.pipe(fs.createWriteStream(path));
        res.send('success');
    });
    req.busboy.on('field', function(key, value, keyTruncated, valueTruncated) {

    });
    req.pipe(req.busboy);
});


server.listen( 3015, function() {
	return console.log('Listening on port ' + 3015);
});



// start the bash listener here

var spawn = require('child_process').spawn;
var watcher = spawn("./swiftqueue.sh");

watcher.stdout.on('data', function (data) {
    var buff = new Buffer(data);
    console.log("WATCHER LOG: " + buff.toString('utf8'));
});


